package server

import (
	"GRPC-GOkos/building/proto/buildingpb"
	"context"
	"log"
)

type BuildingServer struct {
}

func (b BuildingServer) StoreUpdate(ctx context.Context, request *buildingpb.StoreUpdateRequest) (*buildingpb.BuildingResponse, error) {
	panic("implement me")
}

func (b BuildingServer) Fetch(empty *buildingpb.Empty, server buildingpb.BuildingService_FetchServer) error {
	for  i := 0; i < 3; i++  {
		err:=server.Send(&buildingpb.BuildingResponse{
			Buildings: &buildingpb.Building{
				Id:      string(i),
				Uuid:    "13121",
				Name:    "wira",
				Address: "9932",
				UserId:  "32",
				Status:  "1",
				ListTags: &buildingpb.ListTags{
					 ListTag: []string{
					 	"sx",
					 	"xf",
					 	"mkas",
					 },
				},
			},

		})

		if err != nil {
			log.Fatalf("Error when send response %v",err)
			break
		}
	}

	return nil
}

func (b BuildingServer) Show(ctx context.Context, request *buildingpb.ShowRequest) (*buildingpb.BuildingResponse, error) {
	panic("implement me")
}
