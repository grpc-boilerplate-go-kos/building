package main

import (
	"GRPC-GOkos/building/proto/buildingpb"
	"GRPC-GOkos/building/server"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main()  {
	fmt.Println("Server is Running")
	lis,err:=net.Listen("tcp","0.0.0.0:5001")
	if err != nil {
		log.Fatalf("Fail to listen: %v",err)
	}

	gs:=grpc.NewServer()
	buildingpb.RegisterBuildingServiceServer(gs,&server.BuildingServer{})
	gs.Serve(lis)
}